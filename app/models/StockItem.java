package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;

@Entity
public class StockItem extends Model {
	
	@Id
    public Long id;
	
	@ManyToOne
    public Warehouse warehouse;
	
	@ManyToOne	
    public Product product;      
	
    public Long quantity;
 
    public String toString() {
        return String.format("$d %s", quantity, product);
    }
}