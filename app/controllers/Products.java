package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.list;
import models.Product;
import java.util.List;
import play.data.Form;
import views.html.products.details;
import models.Tag;
import play.mvc.*;
import play.*;
import java.lang.*;
import views.html.*;
import java.util.ArrayList;
import play.db.ebean.Model;
import com.avaje.ebean.Ebean;
import models.StockItem;

public class Products extends Controller {
	public static Result list(){ // list all products
		List<Product> products = Product.findAll();
		return ok(list.render(products));
	}
	
	private static final Form<Product> productForm = Form.form(Product.class);
	public static Result newProduct(){ // Show a blank product form
		return ok(details.render(productForm));
	}
	
	public static Result details(Product product){ // Show a product edit form
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
			return ok(details.render(filledForm));
		}
		
	public static Result save(){ //save a product
		Form<Product> boundForm = productForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		}		
		Product product = boundForm.get();
		
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;
		
		if (product.id == null) {
			Ebean.save(product);
		} else {
			product.update();
		}

		StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
		
		Ebean.save(product);
		stockItem.save();
		
		flash("success", String.format("Successfully added product %s", product));
		return redirect(routes.Products.list());
	}
	
	public static Result delete(String ean) {
		final Product product = Product.findByEan(ean);
		if(product == null) {
			return notFound(String.format("Product %s does not exists.", ean));
		}
		product.delete();
		return redirect(routes.Products.list());
	}
	
}